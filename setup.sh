#!/bin/bash

echo "Downloading and extracting setup files..."
curl -fsSL -o erebus.tar.gz  "https://gitlab.com/althalus86/erebus/-/archive/master/erebus-master.tar.gz"
tar -zxvf erebus.tar.gz
rm -f erebus.tar.gz
cd erebus-master
echo "Starting installation .... "
sh install.sh

