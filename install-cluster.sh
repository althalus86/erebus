#!/bin/bash

## Required variables:
#RANCHER_DOMAIN=
#RANCHER_PASSWORD=
#CLUSTER_NAME=

RANCHER_URL=https://$RANCHER_DOMAIN:8443

#Shamelessly copied from https://gist.github.com/superseb/c363247c879e96c982495daea1125276

while ! curl -k $RANCHER_URL/ping; do sleep 3; echo "Waiting for Rancher server to be available. This is normal please wait a bit."; done

# Login
LOGINRESPONSE=`curl -s "$RANCHER_URL/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"admin"}' --insecure`
LOGINTOKEN=`echo $LOGINRESPONSE | jq -r .token`

# Change password
curl -s "$RANCHER_URL/v3/users?action=changepassword" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary '{"currentPassword":"admin","newPassword":"'$RANCHER_PASSWORD'"}' --insecure
echo "New Rancher password has been set."

# Login
#LOGINRESPONSE=`curl -s "$RANCHER_URL/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"'$RANCHER_PASSWORD'"}' --insecure`
#LOGINTOKEN=`echo $LOGINRESPONSE | jq -r .token`

# Create API key
APIRESPONSE=`curl -s "$RANCHER_URL/v3/token" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary '{"type":"token","description":"automation"}' --insecure`
# Extract and store token
APITOKEN=`echo $APIRESPONSE | jq -r .token`

# Set server-url
#RANCHER_SERVER=https://your_rancher_server.com
curl -s "$RANCHER_URL/v3/settings/server-url" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" -X PUT --data-binary '{"name":"server-url","value":"'$RANCHER_URL'"}' --insecure > /dev/null
echo "Rancher server url has been set to $RANCHER_URL"

# Create cluster
BINARY_DATA=`echo '{"dockerRootDir":"/var/lib/docker","enableNetworkPolicy":false,"type":"cluster","rancherKubernetesEngineConfig":{"addonJobTimeout":30,"ignoreDockerVersion":true,"sshAgentAuth":false,"kubernetesVersion":"v1.18.12-rancher1-1","type":"rancherKubernetesEngineConfig","authentication":{"type":"authnConfig","strategy":"x509"},"network":{"type":"networkConfig","plugin":"canal"},"ingress":{"type":"ingressConfig","provider":"nginx"},"monitoring":{"type":"monitoringConfig","provider":"metrics-server"},"services":{"type":"rkeConfigServices","kubeApi":{"podSecurityPolicy":false,"type":"kubeAPIService"},"etcd":{"snapshot":false,"type":"etcdService","extraArgs":{"heartbeat-interval":500,"election-timeout":5000}}}},"name":"'$CLUSTER_NAME'"}'`
CLUSTERRESPONSE=`curl -s "$RANCHER_URL/v3/cluster" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary ''$BINARY_DATA'' --insecure`
# Extract clusterid to use for generating the docker run command
CLUSTERID=`echo $CLUSTERRESPONSE | jq -r .id`
echo "Created a new cluster with id: $CLUSTERID"

# Create token
curl -s "$RANCHER_URL/v3/clusterregistrationtoken" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary '{"type":"clusterRegistrationToken","clusterId":"'$CLUSTERID'"}' --insecure > /dev/null

# Set role flags
ROLEFLAGS="--etcd --controlplane --worker"

# Generate nodecommand
AGENTCMD=`curl -s $RANCHER_URL'/v3/clusterregistrationtoken?id="'$CLUSTERID'"' -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --insecure | jq -r '.data[].nodeCommand' | head -1`

# Concat commands
DOCKERRUNCMD="$AGENTCMD $ROLEFLAGS"

# Echo command
echo "Running: $DOCKERRUNCMD"
$DOCKERRUNCMD


while [ "$STATUS" != "active" ]
do 
	STATUS=`curl -s "$RANCHER_URL/v3/clusters/$CLUSTERID" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --insecure | jq -r .state `
	echo "Current Status: $STATUS. Waiting ...."
	sleep 60
done

# Install Longhorn
LONGHORN_DATA=`echo '{"charts":[{"chartName":"longhorn-crd","version":"1.0.201","releaseName":"longhorn-crd","projectId":null,"values":{"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"'$CLUSTER_NAME'","systemDefaultRegistry":""},"systemDefaultRegistry":""}},"annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"}},{"chartName":"longhorn","version":"1.0.201","releaseName":"longhorn","annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"},"values":{"defaultSettings":{"defaultReplicaCount":1},"image":{"defaultImage":true},"persistence":{"defaultClassReplicaCount":1},"privateRegistry":{"registryPasswd":"'$RANCHER_PASSWORD'"},"longhorn":{"default_setting":true},"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"vltc","systemDefaultRegistry":""},"systemDefaultRegistry":""}}}],"noHooks":false,"timeout":"600s","wait":true,"namespace":"longhorn-system","projectId":null,"disableOpenAPIValidation":false,"skipCRDs":false}'`
LONGHORN_RESPONSE=`curl -s "$RANCHER_URL/k8s/clusters/$CLUSTERID/v1/catalog.cattle.io.clusterrepos/rancher-charts?action=install" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary ''$LONGHORN_DATA'' --insecure`

# Install monitoring
#MONITORING_DATA=`echo '{"charts":[{"chartName":"rancher-monitoring-crd","version":"9.4.201","releaseName":"rancher-monitoring-crd","values":{"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"'$CLUSTER_NAME'","systemDefaultRegistry":""},"systemDefaultRegistry":""}},"annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"}},{"chartName":"rancher-monitoring","version":"9.4.201","releaseName":"rancher-monitoring","annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"},"values":{"prometheus":{"prometheusSpec":{"evaluationInterval":"1m","retentionSize":"50GiB","scrapeInterval":"1m"}},"rkeControllerManager":{"enabled":true},"rkeEtcd":{"enabled":true},"rkeProxy":{"enabled":true},"rkeScheduler":{"enabled":true},"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"'$CLUSTER_NAME'","systemDefaultRegistry":""},"systemDefaultRegistry":""}}}],"noHooks":false,"timeout":"600s","wait":true,"namespace":"cattle-monitoring-system","disableOpenAPIValidation":false,"skipCRDs":false}'`
#MONITORING_RESPONSE=`curl -s "$RANCHER_URL/k8s/clusters/$CLUSTERID/v1/catalog.cattle.io.clusterrepos/rancher-charts?action=install" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary ''$MONITORING_DATA'' --insecure`

# Install istio
#ISTIO_DATA=`echo '{"charts":[{"chartName":"rancher-kiali-server-crd","version":"1.24.001","releaseName":"rancher-kiali-server-crd","values":{"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"'$CLUSTER_NAME'","systemDefaultRegistry":""},"systemDefaultRegistry":""}},"annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"}},{"chartName":"rancher-istio","version":"1.7.300","releaseName":"rancher-istio","annotations":{"catalog.cattle.io/ui-source-repo-type":"cluster","catalog.cattle.io/ui-source-repo":"rancher-charts"},"values":{"global":{"cattle":{"clusterId":"'$CLUSTERID'","clusterName":"'$CLUSTER_NAME'","systemDefaultRegistry":""},"systemDefaultRegistry":""}}}],"noHooks":false,"timeout":"600s","wait":true,"namespace":"istio-system","disableOpenAPIValidation":false,"skipCRDs":false}'`
#ISTIO_RESPONSE=`curl -s "$RANCHER_URL/k8s/clusters/$CLUSTERID/v1/catalog.cattle.io.clusterrepos/rancher-charts?action=install" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary ''$ISTIO_DATA'' --insecure`

#get kube config
docker run --rm --net=host -v $(docker inspect kubelet --format '{{ range .Mounts }}{{ if eq .Destination "/etc/kubernetes" }}{{ .Source }}{{ end }}{{ end }}')/ssl:/etc/kubernetes/ssl:ro --entrypoint bash $(docker inspect $(docker images -q --filter=label=org.label-schema.vcs-url=https://github.com/rancher/hyperkube.git) --format='{{index .RepoTags 0}}' | tail -1) -c 'kubectl --kubeconfig /etc/kubernetes/ssl/kubecfg-kube-node.yaml get configmap -n kube-system full-cluster-state -o json | jq -r .data.\"full-cluster-state\" | jq -r .currentState.certificatesBundle.\"kube-admin\".config | sed -e "/^[[:space:]]*server:/ s_:.*_: \"https://127.0.0.1:6443\"_"' > ~/.kube/config


istioctl install --set profile=default -y

### need to check that all apps are ready
echo "Waiting for 2 minutes to install longhorn ...."
sleep 120