#!/bin/bash

echo 'Please set required variables ::'
echo
[ -z "$DYDNS_USERNAME" ] && read -p 'DYDNS_USERNAME: ' DYDNS_USERNAME
[ -z "$DYDNS_PASSWORD" ] && read -p 'DYDNS_PASSWORD: ' DYDNS_PASSWORD
[ -z "$RANCHER_WITH_CERTIFICATE" ] && read -p 'RANCHER_WITH_CERTIFICATE: ' RANCHER_WITH_CERTIFICATE
[ -z "$CLUSTER_NAME" ] && read -p 'CLUSTER_NAME: ' CLUSTER_NAME
[ -z "$RANCHER_PASSWORD" ] && read -p 'RANCHER_PASSWORD: ' RANCHER_PASSWORD
[ -z "$RANCHER_DOMAIN" ] && read -p 'RANCHER_DOMAIN: ' RANCHER_DOMAIN
[ -z "$CLOUDFLARE_API_KEY" ] && read -p 'CLOUDFLARE_API_KEY: ' CLOUDFLARE_API_KEY
[ -z "$ARGO_CD_PASS" ] && read -p 'ARGO_CD_PASS: ' ARGO_CD_PASS
[ -z "$REPO_ACCESS_TOKEN" ] && read -p 'REPO_ACCESS_TOKEN: ' REPO_ACCESS_TOKEN
echo 'All variables already set. Starting installation....'
echo