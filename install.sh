#!/bin/bash

echo
echo 'Starting installation script. The whole install should take around 30 mins.'
echo '==========================================================================='

[ -f variables.sh ] && { source ./variables.sh; echo "loaded some variables from file..."; }
[ -f ../variables.sh ] && { source ../variables.sh; echo "loaded some variables from file..."; }
source ./install-variables.sh
source ./install-bootstrap.sh
source ./install-cluster.sh
source ./install-post.sh