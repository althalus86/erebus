#!/bin/bash

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#                  Start Bootstrap                       #
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

# Start installation of dependancies 
sudo yum install -y yum-utils epel-release iscsi-initiator-utils container-selinux selinux-policy-base ntp ntpdate wget net-tools jq iotop certbot httpd-tools

#set ntp
systemctl start ntpd
systemctl enable ntpd
ntpdate -u -s 0.centos.pool.ntp.org 1.centos.pool.ntp.org 2.centos.pool.ntp.org
systemctl restart ntpd
hwclock  -w 

# Install docker
if ! type "docker" > /dev/null; then
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install -y docker-ce docker-ce-cli containerd.io htop
    sudo systemctl start docker
    sudo systemctl enable docker
fi

# Install & setup dyndns
rpm -ivh https://www.dynu.com/support/downloadfile/30
cat <<EOF > /etc/dynuiuc/dynuiuc.conf
username $DYDNS_USERNAME
password $DYDNS_PASSWORD
location sbc
ipv4 true
ipv6 true
pollinterval 120
debug false
quiet true
EOF
systemctl enable dynuiuc.service
systemctl restart dynuiuc.service

#setup firewall rules
sudo firewall-cmd --zone=public --permanent --add-port=80/tcp
sudo firewall-cmd --zone=public --permanent --add-port=443/tcp
sudo firewall-cmd --zone=public --add-forward-port=port=8000:proto=tcp:toport=80:toaddr=192.168.1.100 --permanent #this is the loopback to reach the device
#sudo firewall-cmd --zone=public --add-forward-port=port=80:proto=tcp:toport=8080 --permanent #this is for acme to work in rancher
sudo firewall-cmd --zone=public --add-forward-port=port=80:proto=tcp:toport=81 --permanent #this is for certbot to work when renewing
service firewalld restart

#add getting certs
[ ! -f /etc/letsencrypt/live/$RANCHER_DOMAIN/fullchain.pem ] && sudo certbot certonly --http-01-port 81 --standalone --agree-tos --non-interactive -d $RANCHER_DOMAIN -m $DYDNS_USERNAME
echo "0 */12 * * * certbot renew --http-01-port 81" > /var/spool/cron/root

#install CLIs

#Add repo for kubectl
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

if ! type "kubectl" > /dev/null; then
    sudo yum install -y kubectl 
fi

#helm
if ! type "helm" > /dev/null; then
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
    chmod 700 get_helm.sh
    ./get_helm.sh  
fi

#argocd
if ! type "argocd" > /dev/null; then
    VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')
    curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64
    chmod +x /usr/local/bin/argocd
fi

#istio
if ! type "istioctl" > /dev/null; then
    #curl -sL https://istio.io/downloadIstioctl | sh -
    #echo "PATH=$PATH:$HOME/.istioctl/bin" >> ~/.bashrc #to be available on next shell session
    #export PATH=$PATH:$HOME/.istioctl/bin #adding it to the current shell
    curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.6.13 TARGET_ARCH=x86_64 sh -
    echo "PATH=$PATH:/root/istio-1.6.13/bin" >> ~/.bashrc #to be available on next shell session
    export PATH="$PATH:/root/istio-1.6.13/bin"
fi

#Finally installing rancher
if [ ! "$(docker ps -q -f name=rancher_host)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=rancher_host)" ]; then
        # cleanup
        docker rm rancher_host
    fi
    # run rancher
    if [ "$RANCHER_WITH_CERTIFICATE" = "yes" ]; then
        sudo docker run -d --privileged --restart=unless-stopped -v /opt/rancher:/var/lib/rancher -p 8080:80 -p 8443:443 --name rancher_host -v "/etc/letsencrypt/live/$RANCHER_DOMAIN/fullchain.pem:/etc/rancher/ssl/cert.pem" -v "/etc/letsencrypt/live/$RANCHER_DOMAIN/privkey.pem:/etc/rancher/ssl/key.pem" rancher/rancher --no-cacerts
    else
        sudo docker run -d --privileged --restart=unless-stopped -v /opt/rancher:/var/lib/rancher -p 8080:80 -p 8443:443 --name rancher_host rancher/rancher
    fi
fi

