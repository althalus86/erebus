# Erebus

Erebus is a CentOS specific script that instantiates a k8s cluster using rancher and is targeted to run everything on one machine.

## How to install

curl -fsSL -o erebus.tar.gz  "https://gitlab.com/althalus86/erebus/-/archive/master/erebus-master.tar.gz"
tar -zxvf erebus.tar.gz
rm -f erebus.tar.gz
cd erebus-master
sh install.sh

